//
// Created by vlutsyk on 11/15/14.
// Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Student;


@interface Group : NSObject
@property (assign) int identifire;
@property (strong) NSString *title;
@property (strong) NSMutableArray *students;

-(id)initWithTitle:(NSString *)title;
-(bool)removeStudenWith:(int)identifire;
-(void)pushStudentWith:(Student *)student;
@end