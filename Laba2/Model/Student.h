//
// Created by vlutsyk on 11/15/14.
// Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Student : NSObject
@property (assign) int identifire;
@property (strong) NSString *FIO;
@property (assign) int age;
@property (strong) NSString *phone;

-(id)initWithFIO:(NSString*)fio age:(int)age phone:(NSString *)phone;
@end