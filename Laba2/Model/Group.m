//
// Created by vlutsyk on 11/15/14.
// Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import "Group.h"
#import "Student.h"


@implementation Group
    @synthesize title = _title;
    @synthesize identifire = _identifire;

    -(id)initWithTitle:(NSString *)title {
        if (self = [super init]) {
            self.title = title;
        }
        return self;
    }

    -(void)pushStudentWith:(Student *)student {
        [self.students addObject:student];
    }

    -(bool)removeStudenWith:(int)identifire {
        for (int i = 0; i < self.students.count; ++i) {
            //TODO: debug
            NSLog(@"Remove %i", identifire);
        }
        return true;
    }
@end