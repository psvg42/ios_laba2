//
// Created by vlutsyk on 11/15/14.
// Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import "Student.h"


@implementation Student

    -(id)initWithFIO:(NSString*)fio age:(int)age phone:(NSString *)phone {
        if (self = [super init]) {
            self.FIO = fio;
            self.age = age;
            self.phone = phone;
        }
        return self;
    }

@end