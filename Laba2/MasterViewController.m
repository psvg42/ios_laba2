//
//  MasterViewController.m
//  Laba2
//
//  Created by vlutsyk on 11/15/14.
//  Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import "MasterViewController.h"

#import "DetailViewController.h"
#import "Group.h"
#import "Student.h"
#import "StudentInfoView.h"

@interface MasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation MasterViewController

- (void)awakeFromNib
{
    /*if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.preferredContentSize = CGSizeMake(320.0, 600.0);
    }*/
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }

    Group *group1 = [[Group alloc] initWithTitle:@"Mif11"];
    Group *group2 = [[Group alloc] initWithTitle:@"Mif21"];
    Group *group3 = [[Group alloc] initWithTitle:@"Mif31"];

    [group1 pushStudentWith:[[Student alloc] initWithFIO:@"Vasya Pupkin" age:21 phone:@"+3809712312312"]];
    [group1 pushStudentWith:[[Student alloc] initWithFIO:@"Dima Lalkin" age:22 phone:@"+380965345345"]];
    [group2 pushStudentWith:[[Student alloc] initWithFIO:@"Julia Mamaeva" age:20 phone:@"+3809745423423"]];
    [group2 pushStudentWith:[[Student alloc] initWithFIO:@"Studen Nomer1" age:19 phone:@"+3809714234234"]];
    [group3 pushStudentWith:[[Student alloc] initWithFIO:@"Studen Nomer2" age:20 phone:@"+3809776752342"]];
    [group3 pushStudentWith:[[Student alloc] initWithFIO:@"Studen Nomer3" age:21 phone:@"+3809712342342"]];
    [group3 pushStudentWith:[[Student alloc] initWithFIO:@"Studen Nomer4" age:22 phone:@"+3809715345345"]];
    [group3 pushStudentWith:[[Student alloc] initWithFIO:@"Studen Nomer5" age:23 phone:@"+3809714234123"]];


    [_objects insertObject:group1 atIndex:0];
    [_objects insertObject:group2 atIndex:0];
    [_objects insertObject:group3 atIndex:0];

    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    self.navigationItem.rightBarButtonItem = addButton;

    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    /*if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];*/
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    Group *object = _objects[indexPath.row];
    cell.textLabel.text = [object title];

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        NSDate *object = _objects[indexPath.row];
        self.detailViewController.view.tab
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSDate *object = _objects[indexPath.row];
        [[segue destinationViewController] setDetailItem:object];
    }
}

@end