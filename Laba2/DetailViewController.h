//
//  DetailViewController.h
//  Laba2
//
//  Created by vlutsyk on 11/15/14.
//  Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) NSMutableArray *students;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end