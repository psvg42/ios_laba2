//
//  MasterViewController.h
//  Laba2
//
//  Created by vlutsyk on 11/15/14.
//  Copyright (c) 2014 ___KSUN&PASHTET___. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end